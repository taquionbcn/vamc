--------------------------------------------------------------------------------
--  UMass , Physics Department
--  Guillermo Loustau de Linares
--  guillermo.ldl@cern.ch
--------------------------------------------------------------------------------
--  Project: ATLAS L0MDT Trigger 
--  Module: Configurable delay pipeline
--  Description:
--
--------------------------------------------------------------------------------
--  Revisions:
--      simple with no apb controller : done , seems to work ok
--      simple with apb controller : not done
--      parallel mems with apb controller : doing
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library shared_lib;
use shared_lib.common_ieee.all;

library vamc_lib;
use vamc_lib.vamc_pkg.all;


entity vamc_ctrl_ext is
  generic(
    g_PARALLEL_MEM      : integer := 1
  );
  port (

    clk                 : in std_logic;
    rst                 : in std_logic;
    ena                 : in std_logic;
    --
    i_freeze            : in std_logic;
    i_apb_freeze        : in std_logic; --_vector(g_PARALLEL_MEM downto 0);
    i_sel_apb_mem       : in std_logic_vector(3 downto 0);--integer range 0 to g_PARALLEL_MEM;
    --
    o_freeze            : out std_logic_vector(g_PARALLEL_MEM downto 0);
    o_apb_sel_v         : out std_logic_vector(g_PARALLEL_MEM downto 0);
    -- o_apb_sel_i         : out std_logic_vector(3 downto 0);
    o_sel_run           : out integer range 0 to g_PARALLEL_MEM;
    o_sel_apb           : out integer range 0 to g_PARALLEL_MEM
  );
end entity vamc_ctrl_ext;

architecture beh of vamc_ctrl_ext is
  signal sel_apb_mem           : integer range 0 to g_PARALLEL_MEM;

  signal sel_run           : integer range 0 to g_PARALLEL_MEM;
  signal sel_apb           : integer range 0 to g_PARALLEL_MEM;
  
begin
  sel_apb_mem <= to_integer(unsigned(i_sel_apb_mem));
  o_sel_run <= sel_run;
  o_sel_apb <= sel_apb;
  sel_ctrl: process(clk)
  begin
    if rising_edge(clk) then
      if rst = '1' then
        sel_run <= 0;
        sel_apb <= g_PARALLEL_MEM;
        o_apb_sel_v <= (others => '0');
        o_freeze <= (others => '0');
      else
        -- freeze ctrl
        if i_freeze then -- full freeze
          o_freeze <= (others => '1');
        else
          if i_apb_freeze then -- single freeze
            if g_PARALLEL_MEM > 0 then
              if sel_apb_mem <= g_PARALLEL_MEM then
                o_freeze(sel_apb_mem) <= '1';
                o_apb_sel_v(sel_apb_mem) <= '1';
                if(sel_apb_mem = sel_run)then
                  if sel_run < g_PARALLEL_MEM then
                    sel_run <= sel_run + 1;
                  else
                    sel_run <= 0;
                  end if;
                end if;
                sel_apb <= sel_apb_mem;
              else
                -- error
              end if;
            else --g_PARALLEL_MEM = 0 = full freeze
            o_freeze(0) <= '1';
            o_apb_sel_v(0) <= '1';
            end if;

          else -- ALL working
            o_apb_sel_v <= (others => '0');
            o_freeze <= (others => '0');
          end if;
        end if;






        -- -- ext ctrl
        -- if i_freeze then
        --   o_freeze <= (others => '1');
        -- else
        --   if i_apb_freeze then
        --     if g_PARALLEL_MEM > 0 then
        --       if(sel_apb_mem = sel_run)then
        --         if sel_run < g_PARALLEL_MEM then
        --           sel_run <= sel_run + 1;
        --         else
        --           sel_run <= 0;
        --         end if;
        --       end if;
        --       sel_apb <= sel_apb_mem;
        --     else
              
        --     end if;
        --     --
        --     o_freeze(sel_apb) <= '1';
        --     o_apb_sel_v(sel_apb) <= '1';
        --   else
        --     o_apb_sel_v <= (others => '0');
        --     o_freeze <= (others => '0');
        --   end if;
        -- end if;

      end if;
    end if;
  end process sel_ctrl;
  
end architecture beh;