# Vhdl Advanced Memory Controller

Vhdl Advanced Memory Controller is a generic and configurable entity that facilitates the implementation of RAM memory and pipelines and adds functions as different levels of freeze.

![VAMC diagram](/docs/figs/VAMC_full_diag.png)

## Table of configurations

| **generic** | **Types** | **Options**  | **Default** | **Description** |
| --- | :---: | :---: | :---: | :---|
| **Controller Configuration**  |      |       |                 |
| g_SIMULATION | std_logic | '0'=Disabled <br\> '1'=Enabled | '0' | **Simulation patch:** applies some modifications for simulation|
| g_FREEZE_ENABLED | std_logic | '0'=Disabled <br\> '1'=Enabled | '0' | **Freeze Enabler:** freezes all the memory, such that it is not possible to read nor write and stops address automatic incremental logic. |
| g_PARALLEL_MEM | integer | # of memories | 0 | **Parallel memories:** selects the number of parallel memories to instantiate. |
| **Memory Configuration** |  |  |  |  |
| g_MEMORY_MODE | string | "RAM" <br\> "pipeline" | "pipeline" | **Memory Mode:** selects the type of operation the block will perform |
| g_MEMORY_TYPE | string | "auto" <br\> "distributed" <br\> "BRAM" <br\> "URAM" | "auto" | **Memory type:** selects the type of FPGA resource to implement the memory in |
| g_ADDR_WIDTH | integer | # bits  | 0 | **Address Width:** address number of bits |
| g_DATA_WIDTH | integer | # bits  | 0 | **Data width:** number of bits of the data |
| g_DATA_DEPTH | integer | # words | 0 | **Data width:** number of words of the memory (if =! 0 has priority over g_ADDR_WIDTH) |
| **Pipeline Configuration** |  |  |  |  |
| g_PIPELINE_TYPE | string | "auto" <br\> "shift_register" <br\> "ring_buffer" | "auto" | **Pipeline type:** selects the architecture of the pipeline to implement. |
| g_DELAY_CYCLES | integer | # of cycles | 0 | **Delay cycles:** Latency of the pipeline |
| **Secondary Configuration** |  |  |  |  |
| g_EXT_INT | string | "none" <br\> "APB" | "none" | **External interface:** selects the external interface block to read and write via a secondary port. |
| g_XML_NODE_NAME | string | "name"  | "none" | Name of the model: Name of the APB interface, name of the XML file in the regmap |

## Mode of operation

The current version of the VAMC has two main modes of operation: as a RAM memory or as a pipeline.

### RAM

A RAM memory can be implemented using the \gls{XPM} in mode of True Dual Port, so this way can be permanently monitored via an external memory interface.

### Pipeline

Depending on the size and the use of the pipeline to be implemented, there are manly to architectures to be implemented

#### Shift register

This is the most simple implementation.

This is appropriate for:

- small pipelines.
- cross different sector logic regions.

#### Ring Memory
